import React, { Component } from 'react';
import {
  StyleSheet, Dimensions, View, TouchableOpacity, ImageBackground, ScrollView, Image, Modal,
  ActivityIndicator
} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import ETY from 'react-native-vector-icons/Entypo';


/*********************************************** React Navigation *********************************************/
import { createAppContainer, createSwitchNavigator, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems, DrawerActions } from 'react-navigation-drawer';

/*********************************************** Import Page Components *********************************************/
import WelcomePage from '../components/welcome';
import LoginPage from '../components/login';
import HomePage from '../components/home';
import EventPage from '../components/event';
import SchoolActivity from '../components/event/pages/activity';
import SchoolProgram from '../components/event/pages/program';
import EventListContent from '../components/event/pages/content/eventList';
import InforPanel from '../components/event/pages/content/infoPanel';
import SchoolHolidays from '../components/event/pages/holidays';
import GalleryPage from '../components/gallery';
import GalleryList from '../components/gallery/pages/galleryList';
import NotificationPage from '../components/notification';
import NotifiContentList from '../components/notification/pages/notifiContent';
import SchoolStarPage from '../components/schoolStar';
import SchoolStaffPage from '../components/schoolStaff';
import SchoolTimeTablePage from '../components/timeTable';
import ViewTimeTable from '../components/timeTable/pages/timetable';

const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";



/*********************************************** StyleSheet *********************************************/
const styles = StyleSheet.create({
  menustyWarp: { justifyContent: 'center', flex: 1, paddingLeft: 10, paddingRight: 50, height: '100%', },
  menusty: { color: '#fff', },
  menustyClose: { color: '#fff', paddingRight: 20 },
  closeWarp: { height: 70, justifyContent: "center", },
})

/*********************************************** Drawer Button *********************************************/
const DrawerButton = (props) => {
  return (
    <View>
      <TouchableOpacity onPress={props.navigation.toggleDrawer} style={styles.menustyWarp}>
        <ANT name="menu-unfold" size={20} style={styles.menusty} />
      </TouchableOpacity>
    </View>
  );
};

/*********************************************** createStackNavigator *********************************************/

///////////////////////////// HomeStackScreen /////////////////////////////////// 
const HomeStackScreen = createStackNavigator({
  welcomes: {
    screen: WelcomePage,
    navigationOptions: {
      header: null,
    }
  },
  login: {
    screen: LoginPage,
    navigationOptions: {
      header: null,
    }
  },
  home: {
    screen: HomePage,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  school_activity: {
    screen: SchoolActivity,
    navigationOptions: ({ navigation }) => ({
      title: 'School Activity',
    })
  },
  school_program: {
    screen: SchoolProgram,
    navigationOptions: ({ navigation }) => ({
      title: 'School Program',
    })
  },
  school_holidays: {
    screen: SchoolHolidays,
    navigationOptions: ({ navigation }) => ({
      title: 'School Holidays',
    })
  },
  evenlist_content: {
    screen: EventListContent,
  },
  infor_panel: {
    screen: InforPanel,
  },
  gallery: {
    screen: GalleryPage,
    navigationOptions: ({ navigation }) => ({
      title: 'Gallery',
    })
  },
  gallery_list: {
    screen: GalleryList,
  },

}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

///////////////////////////// EventStackScreen /////////////////////////////////// 
const EventStackScreen = createStackNavigator({
  event: {
    screen: EventPage,
    navigationOptions: ({ navigation }) => ({
      title: 'Event',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  school_activity: {
    screen: SchoolActivity,
    navigationOptions: ({ navigation }) => ({
      title: 'School Activity',
    })
  },
  school_program: {
    screen: SchoolProgram,
    navigationOptions: ({ navigation }) => ({
      title: 'School Program',
    })
  },
  school_holidays: {
    screen: SchoolHolidays,
    navigationOptions: ({ navigation }) => ({
      title: 'School Holidays',
    })
  },
  evenlist_content: {
    screen: EventListContent,
  },
  infor_panel: {
    screen: InforPanel,
  },
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

///////////////////////////// ShoolStaffStackScreen /////////////////////////////////// 
const ShoolStaffStackScreen = createStackNavigator({
  school_staff: {
    screen: SchoolStaffPage,
    navigationOptions: ({ navigation }) => ({
      title: 'School Staff',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

///////////////////////////// ShoolStarStackScreen /////////////////////////////////// 
const ShoolStarStackScreen = createStackNavigator({
  shool_star: {
    screen: SchoolStarPage,
    navigationOptions: ({ navigation }) => ({
      title: 'School Star',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

///////////////////////////// NotificationStackScreen /////////////////////////////////// 
const NotificationStackScreen = createStackNavigator({
  notification: {
    screen: NotificationPage,
    navigationOptions: ({ navigation }) => ({
      title: 'Notifications',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  notifi_content: {
    screen: NotifiContentList
  }
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

///////////////////////////// ShoolTimeTableStackScreen /////////////////////////////////// 
const ShoolTimeTableStackScreen = createStackNavigator({
  school_timeTable: {
    screen: SchoolTimeTablePage,
    navigationOptions: ({ navigation }) => ({
      title: 'Student Timetable',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  view_timetable: {
    screen: ViewTimeTable,
    navigationOptions: ({ navigation }) => ({
      title: 'Timetable',
    })
  }
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })


///////////////////////////// GalleryStackScreen /////////////////////////////////// 
const GalleryStackScreen = createStackNavigator({
  gallery: {
    screen: GalleryPage,
    navigationOptions: ({ navigation }) => ({
      title: 'Gallery',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  gallery_list: {
    screen: GalleryList,
  },
},
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#0076C4', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })

/*********************************************** CustomDrawerComponent *********************************************/
const CustomDrawerComponent = (props, screenProps) => {
  return (
    <SafeAreaView forceInset={{ top: 'never', bottom: 'never' }}>
      <View style={styles.closeWarp}>
        <TouchableOpacity onPress={props.navigation.closeDrawer} style={{ width: '100%', alignItems: 'flex-end' }}>
          <ANT name="close" size={30} style={styles.menustyClose} />
        </TouchableOpacity>
      </View>
      <CardItem style={{ backgroundColor: 'transparent', }}>
        <Left style={{ borderBottomWidth: .5, borderBottomColor: '#fff', paddingBottom: 25 }}>
          <Thumbnail source={{ uri: image }} style={{ width: 60, height: 60 }} />
          <Body>
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>SCJK Chong Hua</Text>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <FA name="phone" size={18} style={{ paddingRight: 8, color: '#fff' }} />
              <Text note style={{ color: '#fff' }}>03-1234567</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <ETY name="mail" size={18} style={{ paddingRight: 8, color: '#fff' }} />
              <Text note style={{ color: '#fff' }}>SCJK@mail.com</Text>
            </View>
          </Body>
        </Left>
      </CardItem>
      <ScrollView>
        <DrawerNavigatorItems {...props} />
      </ScrollView>
    </SafeAreaView>
  )
}

/*********************************************** createDrawerNavigator *********************************************/
const DrawerNavigator = createDrawerNavigator({
  home: {
    name: 'Home',
    screen: HomeStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Home',
    })
  },
  event: {
    name: 'Event',
    screen: EventStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Event',
    })
  },
  school_staff: {
    name: 'School Staff',
    screen: ShoolStaffStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'School Staff',
    })
  },
  school_star: {
    name: 'School Star',
    screen: ShoolStarStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'School Star',
    })
  },
  notification: {
    name: 'Notifications',
    screen: NotificationStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Notifications',
    })
  },
  timeTable: {
    name: 'Student Timetable',
    screen: ShoolTimeTableStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Student Timetable',
    })
  },
  gallery: {
    name: 'Gallery',
    screen: GalleryStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Gallery',
    })
  },
}, {
    initialRouteName: 'home',
    contentComponent: props => <CustomDrawerComponent {...props} />,
    drawerType: 'front',
    drawerBackgroundColor: '#0076C4',
    //lazy: 'false',
    drawerWidth: 328,
    drawerPosition: 'left',
    contentOptions: {
      labelStyle: { color: '#ccc', fontSize: 20, fontWeight: 'normal', paddingLeft: 20, },
      activeLabelStyle: { color: '#fff' },
      itemsContainerStyle: { marginTop: 0, },
      // itemStyle: { borderBottomWidth: .5, borderBottomColor: '#ffffff2e', }
    }
  });

/*********************************************** createSwitchNavigator *********************************************/
const AppNavigator = createSwitchNavigator({
  Dashboard: { screen: DrawerNavigator }
});


export default createAppContainer(AppNavigator)