import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { Container, Header, Tab, Tabs, TabHeading, Icon, Text } from 'native-base';



class ImageList extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }


  /*********************************************** _renderGallery *********************************************/
  _renderGallery = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <View style={{ width: '50%', padding: 10 }}>
          <View style={{ height: 200, }}>
            <View style={{ width: '100%', height: 140 }}>
              <Image source={{ uri: item.img }} style={styles.imageSty} />
            </View>
            <View style={styles.contentText}>
              <Text numberOfLines={1} style={styles.textTitle}>{item.name}</Text>
              <Text note numberOfLines={1}>{item.date}</Text>
            </View>
          </View>
      </View>
    )
  }

  render() {
    const dataImage = this.props.data;
    console.log('data----gallery---list', dataImage);
    return (
      <FlatList
            data={dataImage}
            extraData={this.state}
            numColumns={2}
            renderItem={this._renderGallery}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginBottom: 30, }}
          />
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 10, },
  imageSty: { width: '100%', height: '100%', resizeMode: 'cover' },
  contentText: { height: 60, padding: 10, paddingLeft: 0, justifyContent: 'center' },
  textTitle: { marginBottom: 3, color: '#0076C4' },

});

export default ImageList;
