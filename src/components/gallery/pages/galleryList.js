import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Dimensions, FlatList, ScrollView, Modal } from 'react-native';
import { Card, Tab, Tabs, TabHeading, Icon, Text } from 'native-base';
import { WebView } from 'react-native-webview';
import ANT from 'react-native-vector-icons/AntDesign';
import ImageZoom from 'react-native-image-pan-zoom';



const dataImage = [
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
  { image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
]

const dataVideo = [
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video_url: "https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video08.mp4",
    title: 'Video 1', date: '01-05-2020'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video_url: "https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video08.mp4",
    title: 'Video 2', date: '01-05-2020'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video_url: "https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video08.mp4",
    title: 'Video 3', date: '01-05-2020'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video_url: "https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video08.mp4",
    title: 'Video 5', date: '01-05-2020'
  },

]


class GalleryList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.galleryTiTle,
  })
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalData: { video: '' },
      modalPhotoVisible: false,
      modalPhotoData: { image: '' },
    };
  }


  /*********************************************** _renderGalleryImage *********************************************/
  _renderGalleryImage = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <View style={{ width: '50%', padding: 10 }}>
        <TouchableOpacity onPress={() => this.openPhotoModal(item.image)}>
          <View style={{ height: 180, }}>
            <View style={{ width: '100%', height: '100%' }}>
              <Image source={{ uri: item.image }} style={styles.imageSty} />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  /*********************************************** _renderGalleryVideo *********************************************/
  _renderGalleryVideo = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <View style={{ width: '100%', paddingTop: 20 }}>
        <TouchableOpacity onPress={() => this.openModal(item.video_url)}>
          <View style={{ height: 180, position: 'relative' }}>
            <View style={{ width: '100%', height: '100%' }}>
              <Image source={{ uri: item.image }} style={styles.imageSty} />
            </View>
            <View style={{
              position: 'absolute', bottom: 0, padding: 15, paddingLeft: 30,
              backgroundColor: '#0000004a', width: '100%', justifyContent: 'center'
            }}>
              <Text style={{ color: '#fff', marginBottom: 5, fontWeight: 'bold' }}>{item.title}</Text>
              <Text note style={{ color: '#fff' }}>{item.date}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  /***************************************** openModal ****************************************/
  openModal(video_url) {
    const modalData = dataVideo.find(element => {
      return element.video_url === video_url
    })
    this.setState({
      modalVisible: true,
      modalData: { video: modalData.video_url }
    });
  };

  /***************************************** openPhotoModal ****************************************/
  openPhotoModal(image) {
    const modalPhotoData = dataImage.find(element => {
      return element.image === image
    })
    this.setState({
      modalPhotoVisible: true,
      modalPhotoData: { image: modalPhotoData.image }
    });
  };

  /***************************************** closeModal ****************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false,
      modalPhotoVisible: false,
    })
  }

  render() {
    const { modalVisible, modalData, modalPhotoData, modalPhotoVisible } = this.state;
    console.log('modalData', modalData.video)
    return (
      <View style={styles.container}>
        <Tabs>
          <Tab heading={<TabHeading><Icon name="image" /><Text>Photo</Text></TabHeading>}>
            <FlatList
              data={dataImage}
              extraData={this.state}
              numColumns={2}
              renderItem={this._renderGalleryImage}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, }}
            />
          </Tab>
          <Tab heading={<TabHeading><Icon name="camera" /><Text>Video</Text></TabHeading>}>
            <FlatList
              data={dataVideo}
              extraData={this.state}
              renderItem={this._renderGalleryVideo}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, }}
            />
          </Tab>
        </Tabs>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <View style={{ width: '100%', height: 350, backgroundColor: '#000' }}>
              <WebView
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: modalData.video }}
              />
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalPhotoVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <View style={{ width: '100%', height: 350, backgroundColor: '#000' }}>
              <ImageZoom
                cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height - 350}
                imageWidth={Dimensions.get('window').width}
                imageHeight={'100%'}
                pinchToZoom={true}
                panToMove={true}
                enableSwipeDown={true}
                enableCenterFocus={true}
                style={{ backgroundColor: '#000', }}
              >
                <Image source={{ uri: modalPhotoData.image }} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
              </ImageZoom>
            </View>
          </View>
        </Modal>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  imageSty: { width: '100%', height: '100%', resizeMode: 'cover' },
  contentText: { height: 60, padding: 10, paddingLeft: 0, justifyContent: 'center' },
  textTitle: { marginBottom: 3, color: '#0076C4' },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },

});

export default GalleryList;
