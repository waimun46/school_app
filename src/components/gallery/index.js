import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { Card, Text, } from 'native-base';

const data = [
  {
    name: 'Albums TTL1', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL2', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL3', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL4', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw" 
  },
  {
    name: 'Albums TTL5', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL6', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
]

class GalleryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderGallery *********************************************/
  _renderGallery = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <View style={{ width: '50%', padding: 10 }}>
        <TouchableOpacity onPress={() => navigateAction('gallery_list', {galleryTiTle: item.name,})}>
          <View style={{ height: 200, }}>
            <View style={{ width: '100%', height: 140 }}>
              <Image source={{ uri: item.img }} style={styles.imageSty} />
            </View>
            <View style={styles.contentText}>
              <Text numberOfLines={1} style={styles.textTitle}>{item.name}</Text>
              <Text note numberOfLines={1}>{item.date}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <FlatList
            data={data}
            extraData={this.state}
            numColumns={2}
            renderItem={this._renderGallery}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginBottom: 30, }}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 10, },
  imageSty: { width: '100%', height: '100%', resizeMode: 'cover' },
  contentText: { height: 60, padding: 10, paddingLeft: 0, justifyContent: 'center' },
  textTitle: { marginBottom: 3, color: '#0076C4' },

});

export default GalleryPage;
