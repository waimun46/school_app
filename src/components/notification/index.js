import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';

import emergen from '../../assest/icon/notification.png'


const dataNotification = [
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    title: 'Emergency situations ', content: 'School Notification ttl, for Comment oon thee', date: '14-05-2020',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    type: 'emergency'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    title: 'Speech case', content: 'School Notification ttl, for Comment oon thee', date: '14-05-2020',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    type: 'speech'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    title: 'Competitions situations ', content: 'School Notification ttl, for Comment oon thee', date: '14-05-2020',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    type: 'conpetition'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    title: 'Charity case ', content: 'School Notification ttl, for Comment oon thee', date: '14-05-2020',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    type: 'charity'
  },
  {
    image: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    title: 'Emergency situations ', content: 'School Notification ttl, for Comment oon thee', date: '14-05-2020',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
    type: 'emergency'
  },


]

class NotificationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderNoticList *********************************************/
  _renderNoticList = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <List style={styles.listContainer}>
        <ListItem avatar style={{ padding: 5, }} onPress={() => navigateAction('notifi_content', {
          data: item,
          notifi_name: item.title
        })}>
          <Left>
            <Thumbnail source={
              item.type === 'emergency' ? require('../../assest/icon/notification.png') : 
              item.type === 'speech' ? require('../../assest/icon/mic.png') : 
              item.type === 'conpetition' ? require('../../assest/icon/cup.png') : 
              item.type === 'charity' ? require('../../assest/icon/heart-2.png') : null
            }
            />
          </Left>
          <Body style={{ borderBottomWidth: 0 }}>
            <Text note style={styles.titleText}>{item.title}</Text>
            <Text style={{ fontWeight: 'bold' }}>{item.content}</Text>
            <Text note style={{ marginTop: 5, }}>{item.date}</Text>
          </Body>
        </ListItem>
      </List>
    )
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <FlatList
            data={dataNotification}
            extraData={this.state}
            renderItem={this._renderNoticList}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginBottom: 30, }}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  listContainer: { backgroundColor: '#fff', marginTop: 15 },
  titleText: { marginBottom: 5, fontWeight: '500', },

});

export default NotificationPage;
