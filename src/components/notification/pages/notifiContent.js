import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { Text, Card } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";



class NotifiContentList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.notifi_name,
  })
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const nitifiData = this.props.navigation.state.params.data
    console.log('notification', nitifiData)
    return (
      <ScrollView>
        <View style={styles.container}>
          <Card style={{ padding: 20 }}>
            <View>
              <Text style={styles.titleContent}>{nitifiData.content}</Text>
              <Text style={styles.titleSty}>{nitifiData.title}</Text>
              <Text style={styles.descriptionText}>{nitifiData.description}</Text>
            </View>
          </Card>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 10 },
  titleContent: { fontSize: RFPercentage('3.4'), fontWeight: 'bold' },
  titleSty: { color: 'orange', marginTop: 20 },
  descriptionText: { marginTop: 10, lineHeight: 25 },

});

export default NotifiContentList;
