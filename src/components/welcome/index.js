import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';
import ANT from 'react-native-vector-icons/AntDesign';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateAction = this.props.navigation.navigate
    return (
      <ImageBackground source={require('.././../assest/bg/wel_bg.png')} style={styles.backgroundSty}
        imageStyle={{
          resizeMode: 'stretch'
        }}
      >
        <View style={styles.pressContainer}>
          <TouchableOpacity style={styles.pressSty} onPress={() => navigateAction('login')}>
            <Text style={{ color: '#fff', fontSize: RFPercentage('3'), marginRight: 10 }}> ENTER</Text>
            <ANT name="right" size={17} style={styles.iconSty} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundSty: { width: '100%', height: '100%', },
  pressContainer: { flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', bottom: 50, },
  pressSty: { width: '35%', alignItems: 'center', flexDirection: 'row', alignItems: 'center', },
  iconSty: { color: '#fff' }
});

export default WelcomePage;
