import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, FlatList, ScrollView, Image } from 'react-native';
import { Card, Text, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import FEA from 'react-native-vector-icons/Feather';
import { Select, Option } from "react-native-chooser";


const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


const data = [
  {
    name: 'Albums TTL1', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'principal'
  },
  {
    name: 'Albums TTL2', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'vice principal'
  },
  {
    name: 'Albums TTL3', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'gd44'
  },
  {
    name: 'Albums TTL4', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'gd01'
  },
  {
    name: 'Albums TTL5', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'gd01'
  },
  {
    name: 'Albums TTL6', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw", type: 'gd01'
  },
]

class SchoolStaffPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Filter Teacher"
    };
  }

  onSelect(value, label) {
    this.setState({ value: value });
  }


  /*********************************************** _renderStaff *********************************************/
  _renderStaff = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <Card style={[styles.cardContainer, {
        backgroundColor: item.type === 'principal' ? '#F2FAFF' :
          item.type === 'vice principal' ? '#F2FAFF' :
            item.type === 'gd44' ? '#F2FAFF' : '#F2FAFF'
      }]}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ width: '40%', height: 190, }}>
            <Image source={{ uri: item.img }} style={styles.cardImage} />
          </View>
          <View style={{ width: '60%', padding: 10, paddingLeft: 20 }}>
            <View style={{ marginBottom: 10 }}>
              <Text style={{ marginBottom: 5, fontWeight: 'bold' }}>Danny Wong</Text>
              <Text note style={{ color: '#000' }}>Principal</Text>
            </View>

            <View >
              <Text style={{ fontWeight: 'bold', marginBottom: 5, color: 'gray' }}>Project Leader</Text>
              <View style={{ marginBottom: 5 }}>
                <Text note style={{ color: '#000', marginBottom: 3 }}>- Project / Subject</Text>
                <Text note style={{ color: '#000', marginBottom: 3 }}>- Project / Subject</Text>
                <Text note style={{ color: '#000', marginBottom: 3 }}>- Project / Subject</Text>
                <Text note style={{ color: '#000', marginBottom: 3 }}>- Project / Subject</Text>
              </View>
            </View>
          </View>
        </View>
      </Card>
    )
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>

          {/************************* TOP SCHOOL CONTENT INFO *************************/}
          <ImageBackground source={require('../../assest/bg/st.png')} style={styles.backgroundSty}>
            {/************************* LOGO *************************/}
            <View style={styles.logoContainer}>
              <View style={styles.logoSty}>
                <Image source={{ uri: image }} style={styles.logoImage} />
              </View>
            </View>
            {/************************* CONTENT *************************/}
            <View style={styles.schoolInfo}>
              <Text style={styles.schoolTitle}>The School Name</Text>
              <Text note style={styles.schoolContent}>
                一些学校的自励语录 or 学校的about us。Bla bla blablabla bla blabla bla bla blablablablablabla
                blabla blabla ba la ba la balabala ba ba ba ba.
                </Text>
            </View>
            {/************************* INFO *************************/}
            <View style={styles.infoContainer}>
              <View style={[styles.infoBtn, { borderRightWidth: .5, borderRightColor: '#FFF', }]}>
                <FEA name="phone" size={18} color={'#418b65'} />
                <Text note style={{ color: '#fff', marginLeft: 10 }}>03-12345678</Text>
              </View>
              <View style={styles.infoBtn}>
                <FEA name="mail" size={18} color={'#418b65'} />
                <Text note style={{ color: '#fff', marginLeft: 10 }}>school@mail.com</Text>
              </View>
            </View>
          </ImageBackground>
          <View>
            <Select
              onSelect={this.onSelect.bind(this)}
              defaultText={this.state.value}
              style={styles.selectSty}
              textStyle={styles.selectText}
              backdropStyle={{ backgroundColor: "#00000078" }}
              optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
              animationType='none'
              transparent={true}
              indicator='down'
              indicatorColor='#0076C4'
              indicatorStyle={{ marginRight: 20, }}
            >
              <Option value={{ name: "azhar" }}>Azhar</Option>
              <Option value="johnceena">Johnceena</Option>
              <Option value="undertaker">Undertaker</Option>
              <Option value="Daniel">Daniel</Option>
              <Option value="Roman">Roman</Option>
              <Option value="Stonecold">Stonecold</Option>
              <Option value="Rock">Rock</Option>
              <Option value="Sheild">Sheild</Option>
              <Option value="Orton">Orton</Option>

            </Select>
          </View>

          <View style={{ padding: 20 }}>
            <FlatList
              data={data}
              extraData={this.state}
              renderItem={this._renderStaff}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, }}
            />
          </View>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  backgroundSty: { width: '100%', height: 320, resizeMode: 'cover', position: 'relative' },
  infoBtn: {
    width: '50%', alignItems: 'center', alignItems: 'center', flexDirection: 'row',
    justifyContent: 'center'
  },
  logoContainer: { alignItems: 'center', marginTop: 30, },
  logoSty: { width: 100, height: 100, borderRadius: 100, backgroundColor: '#fff', padding: 2 },
  logoImage: { width: '100%', height: '100%', resizeMode: 'cover', borderRadius: 100 },
  schoolInfo: { alignItems: 'center', padding: 30, paddingTop: 20, },
  schoolTitle: { color: '#fff', fontWeight: 'bold', fontSize: RFPercentage('2.8') },
  schoolContent: { textAlign: 'center', color: '#fff', marginTop: 10, lineHeight: 20 },
  infoContainer: {
    position: 'absolute', bottom: 0, flexDirection: 'row', backgroundColor: '#ffffff42',
    padding: 10, paddingTop: 20, paddingBottom: 20,
  },
  selectSty: {
    borderWidth: 0, width: '100%', height: 50, paddingLeft: 10,
    alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  selectText: { color: '#0076C4', textAlign: 'left', width: '100%', paddingLeft: 20, fontWeight: 'bold' },
  cardContainer: { width: '100%', height: 190, width: '100%', marginBottom: 15, borderRadius: 8 },
  cardImage: {
    width: '100%', height: '100%', resizeMode: 'cover', borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },

});

export default SchoolStaffPage;
