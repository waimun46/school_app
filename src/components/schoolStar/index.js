import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { Text, Card } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const data = [
  {
    name: 'Albums TTL1', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL2', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL3', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL4', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL5', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
  {
    name: 'Albums TTL6', date: '04-05-2019',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    video: "https://www.youtube.com/embed/7mT8El4mBVw"
  },
]

class SchoolStarPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderStar *********************************************/
  _renderStar = ({ item }) => {
    const navigateAction = this.props.navigation.navigate
    return (
      <Card style={{ width: '100%', height: 190, width: '100%', marginBottom: 15, backgroundColor: '#F2FAFF', borderRadius: 8  }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ width: '40%',height: 190, }}>
            <Image source={{ uri: item.img }} style={{ width: '100%', height: '100%', resizeMode: 'cover', borderTopLeftRadius: 8, borderBottomLeftRadius: 8, }} />
          </View>
          <View style={{ width: '60%', padding: 10, paddingLeft: 20 }}>
            <View style={{marginBottom: 10}}>
              <Text style={{marginBottom: 5, fontWeight: 'bold'}}>Danny Wong</Text>
              <Text note style={{color: '#000'}}>Grade Six | CGPA - 4.0</Text>
            </View>
            
            <View >
              <Text  style={{fontWeight: 'bold',marginBottom: 5, color: 'gray'}}>Subject</Text>
              <View style={{ flexDirection: 'row', marginBottom: 5}}>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000',   }}>ENG  A+</Text>
                </View>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 5}}>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 5}}>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 5}}>
                <View style={{ width: '50%', }}>
                  <Text note style={{color: '#000'}}>ENG  A+</Text>
                </View>
              </View>
            </View>
            

          </View>
        </View>

      </Card>
    )
  }


  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{ alignItems: 'center', marginTop: 20 }}>
            <Text style={{ fontSize: RFPercentage('4'), fontWeight: 'bold' }}>Our School Star!</Text>
            <Text style={{ marginTop: 20 }}>Here is our <Text style={{ fontWeight: 'bold' }}>TOP 10</Text> school Star 2019.</Text>
          </View>
          <View style={{ marginTop: 20 }}>
            <FlatList
              data={data}
              extraData={this.state}
              renderItem={this._renderStar}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, }}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 20 },


});


export default SchoolStarPage;
