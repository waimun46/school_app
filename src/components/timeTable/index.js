import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, FlatList, ScrollView } from 'react-native';
import { Text, Card, Accordion } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const dataArray = [
  {
    title: "Grade One",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade One',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade One',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'green',
        title: 'Green Class Head Teacher', grade: 'Grade One',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade One',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade One',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },
  {
    title: "Grade Two",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade Two',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade Two',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'green',
        title: 'Green Class Head Teacher', grade: 'Grade Two',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade Two',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade Two',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },
  {
    title: "Grade Three",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade Three',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade Three',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'green',
        title: 'Green Class Head Teacher', grade: 'Grade Three',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade Three',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade Three',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },
  {
    title: "Grade Four",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade Four',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade Four',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'green',
        title: 'Green Class Head Teacher', grade: 'Grade Four',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade Four',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade Four',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },
  {
    title: "Grade Five",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade Five',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade Five',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'green',
        title: 'Green Class Head Teacher', grade: 'Grade Five',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade Five',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade Five',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },
  {
    title: "Grade Six",
    class: [
      {
        className: 'Yellow Class', teacher: 'Danny Wong', type: 'yellow',
        title: 'Yellow Class Head Teacher', grade: 'Grade Six',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Red Class', teacher: 'Danny Wong', type: 'red',
        title: 'Red Class Head Teacher', grade: 'Grade Six',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Green Class', teacher: 'Danny Wong', type: 'Six',
        title: 'Green Class Head Teacher', grade: 'Grade Six',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Blue Class', teacher: 'Danny Wong', type: 'blue',
        title: 'Blue Class Head Teacher', grade: 'Grade Six',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
      {
        className: 'Purple Class', teacher: 'Danny Wong', type: 'purple',
        title: 'Purple Class Head Teacher', grade: 'Grade Six',
        timetable: 'https://image.slidesharecdn.com/timetabletamilschoolmalaysia-100220005820-phpapp01/95/time-table-tamil-school-malaysia-1-728.jpg?cb=1266627517'
      },
    ]
  },

];


class SchoolTimeTablePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }



  /*********************************************** onChangeScreen *********************************************/
  onChangeScreen(screen, item) {
    const { navigation } = this.props;
    navigation.navigate(screen, { data: item });
  }

  /*********************************************** _renderContent *********************************************/
  _renderContent = item => {
    return (
      <View style={{ backgroundColor: "#F2FAFF", }}>
        <View style={{ alignItems: 'center' }}>
          {
            item.class.map((item, key) => {
              return (
                <TouchableOpacity style={styles.classType}
                  onPress={() => this.onChangeScreen("view_timetable", item)}
                >
                  <Text note style={{
                    color:
                      item.type === 'yellow' ? 'orange' :
                        item.type === 'red' ? 'red' :
                          item.type === 'green' ? 'green' :
                            item.type === 'blue' ? 'blue' :
                              item.type === 'purple' ? 'purple' : null,
                    marginBottom: 5
                  }}>{item.className}</Text>
                  <Text note style={{
                    color:
                      item.type === 'yellow' ? 'orange' :
                        item.type === 'red' ? 'red' :
                          item.type === 'green' ? 'green' :
                            item.type === 'blue' ? 'blue' :
                              item.type === 'purple' ? 'purple' : null
                  }}>{item.teacher}</Text>
                </TouchableOpacity>
              )
            })
          }
        </View>
      </View>
    );
  }

  render() {
    console.log('dataArray', dataArray)
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{ alignItems: 'center', marginTop: 20, padding: 20 }}>
            <Text style={{ fontSize: RFPercentage('4'), fontWeight: 'bold' }}>Welcome!</Text>
            <Text style={{ marginTop: 20 }}>
              You have to select your class for view timetable
          </Text>
          </View>
          <Accordion
            dataArray={dataArray}
            headerStyle={{ backgroundColor: "#fff", padding: 30, paddingTop: 25, paddingBottom: 25, borderBottomWidth: .5, borderBottomColor: '#ccc' }}
            contentStyle={{ backgroundColor: "#F2FAFF", padding: 30 }}
            renderContent={this._renderContent}
          />
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, paddingBottom: 30},
  classType: {
    backgroundColor: '#F2FAFF', width: '100%', padding: 20, alignItems: 'center',
    borderBottomWidth: .5, borderBottomColor: '#ccc',
  },


});

export default SchoolTimeTablePage;
