import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, Dimensions, ScrollView, Modal } from 'react-native';
import { Text, Card, Accordion } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import ImageZoom from 'react-native-image-pan-zoom';

const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


class ViewTimeTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalData: { image: '' },
    };
  }

  /***************************************** openModal ****************************************/
  openModal() {
    this.setState({
      modalVisible: true,
    });
  };

  /***************************************** closeModal ****************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false,
    })
  }

  render() {
    const { modalVisible, modalData, } = this.state;

    const dataTimetable = this.props.navigation.state.params.data
    console.log('timetable-----', dataTimetable)
    return (
      <ScrollView>
        <View style={styles.container}>
          {/************************* TOP TITLE *************************/}
          <View style={styles.titleContainer}>
            <Text style={{ color: '#0076C4' }}>{dataTimetable.grade}, {dataTimetable.className}</Text>
          </View>

          {/************************* TEACHER INRO *************************/}
          <View style={styles.teacherContainer}>
            <View style={{ width: '25%' }}>
              <Image source={{ uri: image }} style={{ width: '100%', height: 100 }} />
            </View>
            <View style={{ width: '75%', padding: 20, justifyContent: 'center' }}>
              <Text style={{ fontWeight: 'bold', marginBottom: 5, }}>{dataTimetable.teacher}</Text>
              <Text>{dataTimetable.title}</Text>
            </View>
          </View>

          {/************************* TIMETABLE *************************/}
          <View style={styles.timetableContainer}>
            <Image source={{ uri: dataTimetable.timetable }} style={styles.timetableImage} />
          </View>

          {/************************* POPUP BUTTON *************************/}
          <View style={{ alignItems: 'flex-end', paddingRight: 20, marginTop: 10, }}>
            <TouchableOpacity onPress={() => this.openModal()} >
              <Text style={{ color: 'orange' }}>Click to popup</Text>
            </TouchableOpacity>
          </View>

        </View>

        {/************************* MODAL *************************/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <View style={styles.modalImage}>
              <ImageZoom
                cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height - 350}
                imageWidth={Dimensions.get('window').width}
                imageHeight={'100%'}
                pinchToZoom={true}
                panToMove={true}
                enableSwipeDown={true}
                enableCenterFocus={true}
                style={{ backgroundColor: '#000', }}
              >
                <Image source={{ uri: dataTimetable.timetable }} style={styles.popupImage} />
              </ImageZoom>
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  titleContainer: {
    padding: 20, backgroundColor: '#fff', alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  teacherContainer: {
    backgroundColor: '#fff', marginTop: 20, flexDirection: 'row',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  popupImage: { width: '100%', height: '100%', resizeMode: 'contain' },
  modalImage: { width: '100%', height: 350, backgroundColor: '#000' },
  timetableContainer: { marginTop: 20, height: 300, width: '100%', padding: 20 },
  timetableImage: { width: '100%', height: '100%', resizeMode: 'cover' },
});

export default ViewTimeTable;
