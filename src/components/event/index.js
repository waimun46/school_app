import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, FlatList, ScrollView } from 'react-native';
import { Card, Text, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


class EventPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateAction = this.props.navigation.navigate
    return (
      <ScrollView>
        <View style={styles.container}>
          {/************************* SCHOOL ACTIVITY *************************/}
          <Card style={styles.cardSty}>
            <TouchableOpacity onPress={() => navigateAction('school_activity')}>
              <ImageBackground source={require('../../assest/bg/sa_event.png')}
                style={styles.backgroundSty}
                imageStyle={{ borderRadius: 8 }}>
                <View style={styles.content}>
                  <Text style={styles.textSty}>School Activity</Text>
                  <Text note style={{ color: '#fff' }}>Lorem Ipsum is simply dummy text.</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </Card>
          {/************************* SCHOOL PROGRAM *************************/}
          <Card style={styles.cardSty}>
            <TouchableOpacity onPress={() => navigateAction('school_program')}>
              <ImageBackground source={require('../../assest/bg/sp_event.png')}
                style={styles.backgroundSty}
                imageStyle={{ borderRadius: 8 }}>
                <View style={styles.content}>
                  <Text style={styles.textSty}>School Program</Text>
                  <Text note style={{ color: '#fff' }}>Lorem Ipsum is simply dummy text.</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </Card>
          {/************************* SCHOOL HOLIDAYS *************************/}
          <Card style={styles.cardSty}>
            <TouchableOpacity onPress={() => navigateAction('school_holidays')}>
              <ImageBackground source={require('../../assest/bg/sh_event.png')}
                style={styles.backgroundSty}
                imageStyle={{ borderRadius: 8 }}>
                <View style={styles.content}>
                  <Text style={styles.textSty}>School Holidays</Text>
                  <Text note style={{ color: '#fff' }}>Lorem Ipsum is simply dummy text.</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </Card>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 20 },
  cardWarp: { padding: 15 },
  cardContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  cardSty: { width: '100%', borderRadius: 8, height: 130, marginBottom: 20 },
  content: { padding: 15 },
  backgroundSty: {
    width: '100%', height: '100%', resizeMode: 'cover', justifyContent: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  textSty: {
    color: '#fff', fontSize: RFPercentage('3.5'), fontWeight: 'bold',
    // textShadowColor: 'rgba(0, 0, 0, 0.75)',
    // textShadowOffset: { width: -1, height: 1 },
    // textShadowRadius: 9
  },
});

export default EventPage;
