import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Platform, ScrollView } from 'react-native';
import { Text, Accordion } from "native-base";

const dataArray = [
  { title: "January", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "February", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "March", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "April", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "May", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "Jun", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "July", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "August", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "September", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "October", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "November", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
  { title: "December", date: '1-1-2020', day: 'Tue', content: "Lorem ipsum dolor sit amet", type: 'Malausia Public Holidays 2020' },
];


class SchoolHolidays extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderContent *********************************************/
  _renderContent(item) {
    return (
      <View style={{ flexDirection: 'row', padding: 30, backgroundColor: "#F2FAFF", }}>
        <View style={{ width: '30%', }}>
          <Text note style={{ color: 'gray' }}>{item.date}</Text>
          <Text note style={{ color: 'gray' }}>{item.day}</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text>{item.content}</Text>
          <Text note style={{ color: 'gray' }}>{item.type}</Text>
        </View>
      </View>

    );
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Accordion
            dataArray={dataArray}
            headerStyle={{ backgroundColor: "#fff",padding: 30, paddingTop: 25, paddingBottom: 25, borderBottomWidth: .5, borderBottomColor: '#ccc' }}
            contentStyle={{ backgroundColor: "#F2FAFF", padding: 30 }}
            renderContent={this._renderContent}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, paddingBottom: 30},

});

export default SchoolHolidays;
