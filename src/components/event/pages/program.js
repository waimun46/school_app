import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


class SchoolProgram extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateAction = this.props.navigation.navigate
    return (
      <ScrollView>
        <View style={styles.container}>

          <View style={styles.flexBoxContainer}>
            {/************************* Extracurricular Tuition *************************/}
            <View style={styles.cardContainer}>
              <TouchableOpacity onPress={() => navigateAction('evenlist_content', { select_name: 'Extracurricular Tuition' })}>
                <Card style={{ borderRadius: 10, width: '100%', alignItems: 'center' }}>
                  <View style={styles.cardWarp}>
                    <View style={styles.cardIcon}>
                      <Image source={require('../../../assest/icon/lesson.png')}  style={styles.cardImage} />
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
              <Text style={styles.cardText}>Extracurricular Tuition</Text>
            </View>
            {/************************* Competition *************************/}
            <View style={styles.cardContainer}>
              <TouchableOpacity onPress={() => navigateAction('evenlist_content', { select_name: 'Competitions' })}>
                <Card style={{ borderRadius: 10, width: '100%', alignItems: 'center' }}>
                  <View style={styles.cardWarp}>
                    <View style={styles.cardIcon}>
                      <Image source={require('../../../assest/icon/student.png')}  style={styles.cardImage} />
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
              <Text note numberOfLines={1} style={styles.cardText}>Competitions</Text>
            </View>
          </View>

          <View style={styles.flexBoxContainer}>
            {/************************* Exam Timetable *************************/}
            <View style={styles.cardContainer}>
              <TouchableOpacity onPress={() => navigateAction('evenlist_content', { select_name: 'Exam Timetable' })}>
                <Card style={{ borderRadius: 10, width: '100%', alignItems: 'center' }}>
                  <View style={styles.cardWarp}>
                    <View style={styles.cardIcon}>
                      <Image source={require('../../../assest/icon/calendar-2.png')}  style={styles.cardImage} />
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
              <Text note style={styles.cardText}>Exam Timetable</Text>
            </View>
          </View>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 15, marginBottom: 50 },
  flexBoxContainer: { flexDirection: 'row', },
  cardContainer: { alignItems: 'center', width: '50%', padding: 10 },
  cardImage: { width: '100%', height: '100%', resizeMode: 'contain' },
  cardText: { marginTop: 10, color: '#0076C4' },
  cardWarp: { padding: 30, paddingBottom: 20, paddingTop: 20 },
  cardIcon: { width: 80, height: 80, padding: 10 }

});

export default SchoolProgram;
