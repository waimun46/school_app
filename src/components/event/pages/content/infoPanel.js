import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Platform, ScrollView, FlatList, Image } from 'react-native';
import { Card, Text, List, ListItem, Body, Badge } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Timeline from 'react-native-timeline-flatlist'



const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


const dataEvent = [
  {
    image: "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
  {
    image: "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
  {
    image: "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
  {
    image: "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
  {
    image: "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
]

const dataTimeLine = [
  { time: '09:00', title: 'Event 1', description: 'Event 1 Description', },
  { time: '10:45', title: 'Event 2', description: 'Event 2 Description' },
  { time: '12:00', title: 'Event 3', description: 'Event 3 Description' },
  { time: '14:00', title: 'Event 4', description: 'Event 4 Description' },
  { time: '16:30', title: 'Event 5', description: 'Event 5 Description' }
]

class InforPanel extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.select_name,
  })
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderInfor *********************************************/
  _renderInfor = ({ item }) => {
    return (
      <Card style={styles.cardListContainer}>
        <View style={styles.backgroundSty}>
          <Image source={{ uri: item.image }} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
        </View>
      </Card>
    )
  }

  /*********************************************** _renderInfor *********************************************/
  _renderInforTimeLine = ({ item }) => {
    return (
      <View style={{ borderBottomWidth: .5, borderBottomColor: '#ccc' }}>
        <View style={{ borderBottomWidth: .5, borderBottomColor: '#ccc' }}>
          <View style={{ padding: 20, paddingTop: 10, paddingBottom: 10 }}>
            <Text note>Today</Text>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <Timeline data={dataTimeLine} circleSize={10} />
        </View>
      </View>
    )
  }

  
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {/************************* TOP IMAGE *************************/}
          <View style={styles.topImageContainer}>
            <Image source={{ url: image }} style={styles.topImage} />
          </View>
          {/************************* HEADER TITLE *************************/}
          <View style={styles.contentContainer}>
            <Badge style={{ backgroundColor: '#0076C4' }}>
              <Text note style={{ color: 'white', fontWeight: 'bold' }}>Donate Blood</Text>
            </Badge>
            <Text style={styles.headerTitle}>
              Help save a Life, Give a gift of blood.
            </Text>
            <Text note style={{ color: '#0076C4', marginTop: 15 }}>20-05-2020</Text>
          </View>
          {/************************* SEE PHOTO *************************/}
          <View >
            <View style={{ padding: 30, paddingBottom: 10, }}>
              <Text style={{ color: 'orange', }}>See Photo</Text>
            </View>
            <FlatList
              data={dataEvent}
              extraData={this.props}
              horizontal={true}
              renderItem={this._renderInfor}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, marginTop: 10 }}
            />
          </View>
          {/************************* SCHEDULE *************************/}
          <View style={[styles.contentContainer, { padding: 0 }]}>
            <View style={{ padding: 30, paddingBottom: 0, paddingTop: 20 }}>
              <Text style={{ color: 'orange', }}>Schedule</Text>
            </View>
            <FlatList
              data={dataEvent}
              extraData={this.props}
              renderItem={this._renderInforTimeLine}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginTop: 10 }}
            />
          </View>
          {/************************* AWARD *************************/}
          <View style={[styles.contentContainer, { paddingTop: 0, paddingBottom: 30, paddingLeft: 0, paddingRight: 0 }]}>
            <View style={{ padding: 30, }}>
              <Text style={{ color: 'orange', marginBottom: 10 }}>Award</Text>
              <Text>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  cardListContainer: { marginRight: 10 },
  backgroundSty: {
    width: 120, height: 120,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  topImageContainer: { width: '100%', height: 200 },
  topImage: { width: '100%', height: '100%', resizeMode: 'cover', },
  contentContainer: { backgroundColor: '#fff', padding: 30 },
  headerTitle: { fontSize: RFPercentage('4'), fontWeight: 'bold', marginTop: 15, },
});

export default InforPanel;
