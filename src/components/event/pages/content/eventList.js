import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Platform, ScrollView, FlatList } from 'react-native';
import { Card, Text, List, ListItem, Body } from 'native-base';

const dataEvent = [
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },
  {
    name: 'School Building', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
    date: '14-05-2020'
  },

]

class EventListContent extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.select_name,
  })
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderList *********************************************/
  _renderList = ({ item }) => {
    // console.log(' item=====', this.props.navigation.state.params.select_name);
    const selectName = this.props.navigation.state.params.select_name;
    const navigateAction = this.props.navigation.navigate;
    return (
      <Card style={styles.cardListContainer}>
        <List>
          <ListItem onPress={() => navigateAction('infor_panel', { select_name: selectName })}>
            <Body>
              <Text note style={{ marginBottom: 5, fontWeight: '500' }}>{item.name}</Text>
              <Text style={{ marginBottom: 5, fontWeight: 'bold' }}>{item.content}</Text>
              <Text note style={{ fontWeight: '500' }}>{item.date}</Text>
            </Body>
          </ListItem>
        </List>
      </Card>
    )
  }



  render() {

    return (
      <ScrollView>
        <View style={styles.container}>
          <FlatList
            data={dataEvent}
            extraData={this.props}
            renderItem={this._renderList}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginBottom: 30, marginTop: 10 }}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  cardListContainer: { marginBottom: 10 },
});

export default EventListContent;
