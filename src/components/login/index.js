import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, TouchableOpacity, TextInput, Dimensions, Image } from 'react-native';
import { Card, CardItem, Text, Body, Item, Label } from 'native-base';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateAction = this.props.navigation.navigate
    return (
      <ImageBackground source={require('../../assest/bg/login_bg.png')} style={styles.backgroundSty}
        imageStyle={{
          resizeMode: 'stretch'
        }}>
        <View style={styles.container}>
          <View style={{ width: 150, height: 150, marginBottom: 20 }}>
            <Image source={require('../../assest/logo/login_logo.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
          </View>
          {/************************* TITLE *************************/}
          <View style={styles.titleSty}>
            <Text>EDUCATION CHANGE THE FUTURE</Text>
          </View>
          {/************************* FORM *************************/}
          <Card style={styles.cardSty}>
            <CardItem>
              <Body>
                <View style={{ width: '100%', padding: 20 }}>
                  <Item stackedLabel style={styles.formContainer}>
                    <Label style={styles.labelSty}>SCHOOL NAME</Label>
                    <TextInput placeholder="Fill in your school name" style={styles.inputStyle} />
                  </Item>
                  <Item stackedLabel style={styles.formContainer}>
                    <Label style={styles.labelSty}>PASSWORD</Label>
                    <TextInput placeholder="Fill in your password" style={styles.inputStyle} />
                  </Item>
                </View>
              </Body>
            </CardItem>
            <CardItem footer style={styles.cardFooter}>
              <TouchableOpacity style={styles.btnContainer} onPress={() => navigateAction('home')}>
                <Text style={styles.textBtn}>Go School </Text>
              </TouchableOpacity>
            </CardItem>
          </Card>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundSty: { width: windowWidth, height: windowHeight, },
  container: { flex: 1, alignItems: 'center', marginTop: 40,  },
  titleSty: { marginBottom: 20 },
  cardSty: { width: '80%' },
  formContainer: { marginBottom: 20, alignItems: 'flex-start', },
  inputStyle: { paddingTop: 10, paddingBottom: 10, width: '100%' },
  btnContainer: { width: '100%', padding: 10 },
  textBtn: { color: '#fff', textAlign: 'center' },
  labelSty: { color: '#0076C4' },
  cardFooter: { backgroundColor: '#0076C4', borderRadius: 0 },
});

export default LoginPage;
