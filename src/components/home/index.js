import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
const data = [
  {
    name: 'Tan Ching Leong', class: '7A', cgpa: '4.0',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Wong Kar Hong', class: '7A', cgpa: '4.0',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lee Ah Beng', class: '6A', cgpa: '3.8',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Kok Mun Fei', class: '4A', cgpa: '3.9',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Adam Lee Wan Ling', class: '4A', cgpa: '4.0',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lucas Lim Hao Long', class: '3A', cgpa: '3.9',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
]
const dataEvent = [
  {
    name: 'Lorem Ipsum is simply dummy text of the printing', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lorem Ipsum is simply dummy text of the printing', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lorem Ipsum is simply dummy text of the printing', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lorem Ipsum is simply dummy text of the printing', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
  {
    name: 'Lorem Ipsum is simply dummy text of the printing', content: 'Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', star: 4.6
  },
]


class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderStarList *********************************************/
  _renderStarList = ({ item }) => {
    return (
      <Card style={styles.starListContainer}>
        <Thumbnail source={{ uri: item.img }} style={{ marginBottom: 10 }} />
        <Text note numberOfLines={1} style={{ color: '#000', marginBottom: 5 }}>{item.name}</Text>
        <Text note numberOfLines={1} style={{ marginBottom: 5 }}>{item.class}</Text>
        <Text note numberOfLines={1} >CGPA: {item.cgpa}</Text>
      </Card>


    )
  }

  /*********************************************** _renderEventList *********************************************/
  _renderEventList = ({ item }) => {
    return (
      <Card style={styles.eventListContainer}>
        <View style={styles.eventImageContainer}>
          <ImageBackground source={{ uri: item.img }}
            imageStyle={{ borderTopLeftRadius: 8, borderTopRightRadius: 8 }}
            style={styles.backgroundSty}
          />
        </View>
        <View style={{ marginLeft: 120, padding: 10, justifyContent: 'center' }}>
          <Text note style={{ color: '#0076C4', marginBottom: 5 }}>{item.name}</Text>
          <Text note numberOfLines={3} style={{ color: '#ccc' }}>{item.content}</Text>
        </View>
      </Card>
    )
  }

  render() {
    const navigateAction = this.props.navigation.navigate
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.cardWarp}>
            {/************************* SELECT SCHOOL ACTION *************************/}
            <View style={styles.cardContainer}>

              <Card style={styles.cardSty}>
                <TouchableOpacity onPress={() => navigateAction('school_program')}>
                  <ImageBackground source={require('../../assest/bg/sp.png')} style={styles.backgroundSty}
                    imageStyle={{
                      resizeMode: 'stretch',
                      borderRadius: 2,
                    }}>
                    <Text style={{ color: '#fff', padding: 10 }}>School Program</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </Card>
              <Card style={styles.cardSty}>
                <TouchableOpacity onPress={() => navigateAction('school_activity')}>
                  <ImageBackground source={require('../../assest/bg/sa.png')} style={styles.backgroundSty}
                    imageStyle={{
                      resizeMode: 'stretch',
                      borderRadius: 2,
                    }}>
                    <Text style={{ color: '#fff', padding: 10 }}>School Activity</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </Card>
            </View>
            <View style={styles.cardContainer}>
              <Card style={styles.cardSty}>
                <TouchableOpacity onPress={() => navigateAction('school_holidays')}>
                  <ImageBackground source={require('../../assest/bg/sp_1.png')} style={styles.backgroundSty}
                    imageStyle={{
                      resizeMode: 'stretch',
                      borderRadius: 2,
                    }}>
                    <Text style={{ color: '#fff', padding: 10 }}>School Holidays</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </Card>
              <Card style={styles.cardSty}>
                <TouchableOpacity onPress={() => navigateAction('gallery')}>
                  <ImageBackground source={require('../../assest/bg/sg.png')} style={styles.backgroundSty}
                    imageStyle={{
                      resizeMode: 'stretch',
                      borderRadius: 2,
                    }}>
                    <Text style={{ color: '#fff', padding: 10 }}>School Gallery</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </Card>
            </View>
          </View>
          {/************************* SCHOOL STAR *************************/}
          <View style={styles.starContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
              <Text style={{ fontSize: RFPercentage('3'), fontWeight: 'bold' }}>School Star 2019</Text>
              <TouchableOpacity onPress={() => navigateAction('shool_star')}>
                <Text style={{ color: 'gray', marginRight: 15 }} >more ></Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={data}
              horizontal={true}
              extraData={this.state}
              renderItem={this._renderStarList}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginTop: 10 }}
            />
          </View>
          {/************************* SCHOOL EVENT *************************/}
          <View style={styles.eventContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
              <Text style={{ fontSize: RFPercentage('3'), fontWeight: 'bold' }}>School Event</Text>
              <TouchableOpacity onPress={() => navigateAction('event')}>
                <Text style={{ color: 'gray', }} >more ></Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={dataEvent}
              extraData={this.state}
              renderItem={this._renderEventList}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ marginBottom: 30, marginTop: 10 }}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  cardWarp: { padding: 15 },
  cardContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  cardSty: {
    width: '48%', height: 80, marginBottom: 15, borderRadius: 5,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  content: { padding: 10 },
  backgroundSty: {
    width: '100%', height: '100%', resizeMode: 'cover',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  textSty: { color: '#fff' },
  starContainer: { marginLeft: 15 },
  starListContainer: {
    marginRight: 10, padding: 10, paddingTop: 15, paddingBottom: 15, alignItems: 'center',
    width: 140, borderRadius: 8
  },
  eventContainer: { marginTop: 10, padding: 15, },
  eventListContainer: {
    borderRadius: 8, height: 120, marginTop: 20, position: 'relative',
    flexDirection: 'row', backgroundColor: '#fff'
  },
  eventImageContainer: {
    height: 130, backgroundColor: '#ccc', bottom: 0, position: 'absolute', width: 110,
    left: 10, borderTopLeftRadius: 8, borderTopRightRadius: 8
  },
  eventImage: { width: '100%', height: '100%', resizeMode: 'cover', },


});

export default HomePage;
