/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React, { Component, Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform
} from 'react-native';
import AppNavigator from './src/navigation';

/*********************************************** StatusBarPlaceHolder *********************************************/
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

function StatusBarPlaceHolder() {
  return (
    <View style={{
      width: "100%",
      height: STATUS_BAR_HEIGHT,
      backgroundColor: "#000"
    }}>
      <StatusBar barStyle="light-content" />
    </View>
  );
}


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Fragment>
        {
          Platform.OS === 'ios' ? <StatusBarPlaceHolder /> :
            <StatusBar barStyle="light-content" backgroundColor="#000" />
        }
        <AppNavigator />
      </Fragment>
    );
  }
}


const styles = StyleSheet.create({

});

export default App;


